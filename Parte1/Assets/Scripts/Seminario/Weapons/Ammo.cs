﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Ammo : MonoBehaviour//, IReusable
{
    public virtual void OnAcquire()
    {
        gameObject.SetActive(true);
    }

    public virtual void OnCreate()
    {
        gameObject.SetActive(false);
    }

    public virtual void OnRelease()
    {
        gameObject.SetActive(false);
    }
}
