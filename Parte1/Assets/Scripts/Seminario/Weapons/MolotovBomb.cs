﻿using UnityEngine;
using System.Collections;
using System;

public class MolotovBomb : Ammo
{
    public GameObject bomb;
    public GameObject fire;

    public override void OnAcquire()
    {
        ActiveMolotov();
    }

    public override void OnCreate()
    {
        TurnDownObjects();
    }

    public override void OnRelease()
    {
        TurnDownObjects();
    }

    private void TurnDownObjects()
    {
        bomb.SetActive(false);
        fire.SetActive(false);
    }

    public void ActiveMolotov()
    {
        bomb.SetActive(true);
        fire.SetActive(false);
    }
    public void StartBurn(Vector3 posi)
    {
        transform.position = posi;
        bomb.SetActive(false);
        fire.SetActive(true);
    }
}
