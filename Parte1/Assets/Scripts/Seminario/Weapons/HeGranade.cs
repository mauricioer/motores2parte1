﻿using UnityEngine;
using System.Collections;

public class HeGranade : Trap
{
    
    //private float coolLig = 0.5f;
    private float currentCool;

    // Use this for initialization
    void Start ()
    {
	
	}

    public override void Update()
    {
        detonTime -= Time.deltaTime;
        if (detonTime <= 0)
            Detonator();
    }

    // Update is called once per frame


    public override void Detonator()
    {
        Explosion();
    }

    public void Explosion()
    {
        // print("explote");

        var cols = Physics.OverlapSphere(transform.position, expRadius, layersDamege);
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].GetComponent<Rigidbody>() != null)
            {
                // print("impact");
                //print(cols[i].gameObject);
                cols[i].GetComponent<Rigidbody>().AddExplosionForce(expPower, transform.position, expRadius, 1f, ForceMode.Impulse);
                if (cols[i].gameObject.layer == K.LAYER_PLAYER)
                    cols[i].gameObject.GetComponentInParent<BuggyData>().Damage(expDamage);

                /*if (cols[i].gameObject.layer == K.LAYER_IA)
                    cols[i].gameObject.GetComponentInParent<IAController>().Damage(expDamage);*/


            }
        }
        Instantiate(feedback, transform.position + transform.up, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
