﻿using UnityEngine;
using System.Collections;

public class FireFloor : MonoBehaviour {

    public float staydamage = 0.01f;
    public float duration = 2f;
    private float _current;

	// Use this for initialization
	void Update()
    {
        _current += Time.deltaTime;
        if (_current > duration)
        {
            _current = 0;
            //PoolManager.instance.bullets.PutBackObject(transform.parent.gameObject);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer == K.LAYER_IA)
        {
            print(col.gameObject);
           // col.GetComponentInParent<IAController>().Damage(staydamage * 20);
        }
    }

    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.layer == K.LAYER_IA)
        {
            //col.GetComponentInParent<IAController>().Damage(staydamage);
        }
    }
}
