﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[ExecuteInEditMode]
public class GraphGenerator : MonoBehaviour
{
    public Node nodePrefab;
    public int width, height;
    public float distanceBetweenNodes;
    public Vector3 initialPivot;
    public bool canCalculate;
    public List<Node> graph = new List<Node>();

    void Update()
    {
        if (!canCalculate) return;
        GenerateGraph();
    }

    public void ClearGraph()
    {
        var destr = transform.GetComponentsInChildren<Node>();
        for (int i = destr.Length - 1; i >= 0; i--) DestroyImmediate(destr[i].gameObject);
        graph.Clear();
    }

    private void GenerateGraph()
    {
        ClearGraph();
        var tempPivot = initialPivot;
        //tempPivot.y = 0.5f;
        for (int w = 0; w < width; w++)
        {
            tempPivot.x = initialPivot.x + w * distanceBetweenNodes;
            for (int h = 0; h < height; h++)
            {
                tempPivot.z = initialPivot.z + h * distanceBetweenNodes;
                RaycastHit rch;
                if (Physics.Raycast(tempPivot + (Vector3.up * 500), -Vector3.up, out rch))
                {
                    if (rch.collider.gameObject.layer == 8)
                    {
                        string tempName = "Node [" + w + "," + h + "]";
                        var tempNode = (GameObject)PrefabUtility.InstantiatePrefab(nodePrefab.gameObject); ;
                        tempNode.name = tempName;
                        tempNode.transform.position = rch.point + Vector3.up;
                        tempNode.transform.parent = transform;
                        graph.Add(tempNode.GetComponent<Node>());
                    }
                }
            }
        }

        foreach (var node in graph)
        {
            var possibleNeighbors = Physics.OverlapSphere(node.transform.position, distanceBetweenNodes + (distanceBetweenNodes * .5f));
            for (int j = 0; j < possibleNeighbors.Length; j++)
            {
                var neighborNode = possibleNeighbors[j].GetComponent<Node>();
                if (neighborNode != null &&
                    possibleNeighbors[j].gameObject != node.gameObject)
                {
                    RaycastHit rh;
                    var dirToNeighbor = possibleNeighbors[j].transform.position - node.transform.position;
                    if (Physics.Raycast(node.transform.position, dirToNeighbor.normalized, out rh, distanceBetweenNodes + (distanceBetweenNodes * .5f)))
                    {
                        if (rh.collider.gameObject.GetComponent<Node>() != null)
                        {
                            node.AddNeighbor(neighborNode);
                        }
                    }
                }
            }
        }
        canCalculate = false;
    }
}
