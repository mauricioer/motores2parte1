﻿using UnityEngine;
using System.Collections;

public enum ComponentList
{
    rigidbody,
    colliders,
    camera,
    audioSource,
    vehicle,
    inputController,
    rigidbody2D
}

public class ComponentSearch : MonoBehaviour
{
    public ComponentList componentToSearch;
}
