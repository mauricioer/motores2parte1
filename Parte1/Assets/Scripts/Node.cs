﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour
{
    public List<Node> neighbors;
    public float nodeRadius;

    public void AddNeighbor(Node neighbor)
    {
        if (neighbors == null) neighbors = new List<Node>();
        neighbors.Add(neighbor);
    }

    void OnDrawGizmos()
    {
        neighbors.RemoveAll(n => n == null);

        if (neighbors != null)
        {
            Gizmos.color = Color.green;
            for (int i = 0; i < neighbors.Count; i++)
            {
                Gizmos.DrawLine(transform.position, neighbors[i].transform.position);
            }
        }
        Gizmos.DrawSphere(transform.position, nodeRadius);
    }
}
