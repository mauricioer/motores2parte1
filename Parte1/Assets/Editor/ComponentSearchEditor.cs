﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(ComponentSearch))]
public class ComponentSearchEditor : Editor
{
    ComponentSearch _target;
    Dictionary<Vehicle, List<Component>> _vehicleDict = new Dictionary<Vehicle, List<Component>>();
    bool _searchButtonClicked, _emptySearch;

    void OnEnable()
    {
        _target = (ComponentSearch)target;
    }

    public override void OnInspectorGUI()
    {
        Show();
        Repaint();
    }

    private void Show()
    {
        _target.componentToSearch = (ComponentList)EditorGUILayout.EnumPopup("Select component", _target.componentToSearch);
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Search"))
        {
            _searchButtonClicked = true;
            _vehicleDict.Clear();
            var keys = _target.GetComponentsInChildren<Vehicle>(true);
            foreach (var item in keys) _vehicleDict.Add(item, new List<Component>());
            switch (_target.componentToSearch)
            {
                case ComponentList.rigidbody:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<Rigidbody>(true));
                    break;
                case ComponentList.colliders:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<Collider>(true));
                    break;
                case ComponentList.camera:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<Camera>(true));
                    break;
                case ComponentList.audioSource:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<AudioSource>(true));
                    break;
                case ComponentList.vehicle:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<Vehicle>(true));
                    break;
                case ComponentList.inputController:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<InputController>(true));
                    break;
                case ComponentList.rigidbody2D:
                    foreach (var key in _vehicleDict.Keys) _vehicleDict[key].AddRange(key.GetComponentsInChildren<Rigidbody2D>(true));
                    break;
                default:
                    break;
            }
        }
        if (GUILayout.Button("Clear"))
        {
            _vehicleDict.Clear();
            _searchButtonClicked = false;
        }
        EditorGUILayout.EndHorizontal();
        foreach (var compList in _vehicleDict.Values)
        {
            if (compList.Count == 0) _emptySearch = true;
            else
            {
                _emptySearch = false;
                break;
            }
        }
        if (_emptySearch && _searchButtonClicked)
        {
            EditorGUILayout.HelpBox("No results found", MessageType.Info);
        }
        else if (_searchButtonClicked)
        {
            foreach (var vehicle in _vehicleDict.Keys)
            {
                if (_vehicleDict[vehicle].Count != 0)
                {
                    EditorGUILayout.Space();
                    EditorGUILayout.Space();
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    EditorGUILayout.LabelField(vehicle.gameObject.name,EditorStyles.centeredGreyMiniLabel);
                    Color guiDefaultColor = GUI.color;
                    GUI.color = vehicle.gameObject.activeSelf ? Color.green : Color.red;
                    if (GUILayout.Button(vehicle.gameObject.activeSelf ? "Active" : "Inactive"))
                    {
                        vehicle.gameObject.SetActive(!vehicle.gameObject.activeSelf);
                    }
                    GUI.color = guiDefaultColor;
                    EditorGUILayout.Space();
                    foreach (var item in _vehicleDict[vehicle])
                    {
                        EditorGUILayout.ObjectField(item.gameObject.name, item, item.GetType(), true);
                    }
                    EditorGUILayout.EndVertical();
                }
            }
        }
    }
}