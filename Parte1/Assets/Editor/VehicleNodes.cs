﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class VehicleNodes : EditorWindow
{
    private Dictionary<GameObject,List<GameObject>> _nodes = new Dictionary<GameObject, List<GameObject>>();
    private List<Rect> _windows = new List<Rect>();
    private Vehicle _vehicle;

    [MenuItem("DeathRush/Vehicle Editor")]
    static void CreateWindow()
    {
        VehicleNodes vn = (VehicleNodes)GetWindow(typeof(VehicleNodes));
        vn.Show();
    }

    private void OnGUI()
    {
        maxSize = new Vector2(800, 600);
        minSize = new Vector2(800, 600);
        _vehicle = (Vehicle)EditorGUILayout.ObjectField("Vehicle", _vehicle, typeof(Vehicle));
        BeginWindows();
        int count = 0;
        Rect window = new Rect(50,50,400,100);
        foreach (var go in _nodes.Keys)
        {
            window = GUI.Window(count,window,DrawNode,go.name+count);
            count++;
        }
        EndWindows();
    }

    private void DrawNode(int id)
    {

        GUI.DragWindow();
    }
}
