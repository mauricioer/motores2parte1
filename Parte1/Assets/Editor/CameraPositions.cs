﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Camera))]
public class CameraPositions : Editor
{
    private Camera _target;
    private List<Vector3[]> _positions = new List<Vector3[]>();

    private void OnEnable()
    {
        _target = (Camera)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        Show();
        EditorGUILayout.EndVertical();
        Repaint();
    }

    private void Show()
    {
        if (GUILayout.Button("Save actual position/rotation"))
        {
            Vector3 pos = _target.transform.position;
            Vector3 rot = _target.transform.eulerAngles;
            Vector3[] vecArray = new Vector3[2] { pos, rot };
            _positions.Add(vecArray);
        }
        if (GUILayout.Button("Clear"))
        {
            _positions.Clear();
        }
        if (_positions.Count == 0) return;
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        foreach (var trans in _positions)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Go To \nPosition/Rotation " + (_positions.IndexOf(trans) + 1)))
            {
                _target.transform.position = trans[0];
                _target.transform.eulerAngles = trans[1];
            }
            Color c = GUI.color;
            GUI.color = Color.red;
            if (GUILayout.Button("Remove"))
            {
                _positions.Remove(trans);
                return;
            }
            GUI.color = c;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Vector3Field("Position", trans[0]);
            EditorGUILayout.Vector3Field("Rotation", trans[1]);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space();
        }
    }
}
