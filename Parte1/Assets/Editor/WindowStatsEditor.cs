﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class WindowStatsEditor : EditorWindow
{
    private GameObject _vehicleContainer;
    private List<Vehicle> _vehicleList = new List<Vehicle>();
    private Vector2 _scrollPos;

    [MenuItem("DeathRush/Stats Editor")]
    static void CreateWindow()
    {
        ((WindowStatsEditor)GetWindow(typeof(WindowStatsEditor))).Show();
    }

    void OnGUI()
    {
        _vehicleContainer = (GameObject)EditorGUILayout.ObjectField("Vehicle Container", _vehicleContainer, typeof(GameObject));
        if (_vehicleContainer == null)
        {
            EditorGUILayout.HelpBox("No container selected", MessageType.Warning);
            return;
        }
        else if (_vehicleContainer.tag != K.TAG_VEHICLES)
        {
            EditorGUILayout.HelpBox("Wrong gameobject selected", MessageType.Error);
            return;
        }
        _vehicleList.Clear();
        _vehicleList.AddRange(_vehicleContainer.GetComponentsInChildren<Vehicle>());
        EditorGUILayout.Space();
        _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height - 30));
        EditorGUILayout.BeginHorizontal();
        foreach (var vehicle in _vehicleList)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.Space();
            vehicle.vehicleName = EditorGUILayout.TextField("Vehicle Name", vehicle.vehicleName);
            var vd = vehicle.GetComponent<VehicleData>();
            vd.maxLife = EditorGUILayout.FloatField("Max Life", vd.maxLife);
            vd.currentLife = EditorGUILayout.FloatField("Current Life", vd.currentLife);
            vehicle.topSpeed = EditorGUILayout.FloatField("Top Speed", vehicle.topSpeed);
            vehicle.downForce = EditorGUILayout.FloatField("Down Force", vehicle.downForce);
            vehicle.maxForce = EditorGUILayout.FloatField("Max Motor Force", vehicle.maxForce);
            vehicle.brakeForce = EditorGUILayout.FloatField("Max Brake Force", vehicle.brakeForce);
            vehicle.centerOfMass.localPosition = EditorGUILayout.Vector3Field("Center of Mass(local)", vehicle.centerOfMass.localPosition);
            vehicle.groundFriction = EditorGUILayout.Slider(vehicle.groundFriction, 0, 1);
            vehicle.airFriction = EditorGUILayout.Slider(vehicle.airFriction, 0, 1);
            vehicle.maximumTurn = EditorGUILayout.FloatField("Maximun Turn Angle", vehicle.maximumTurn);
            vehicle.minimumTurn = EditorGUILayout.FloatField("Minimun Turn Angle", vehicle.minimumTurn);
            vehicle.nitroPower = EditorGUILayout.FloatField("Nitro Power", vehicle.nitroPower);
            string prefabPath = AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(vehicle.gameObject));
            GameObject go = (GameObject)AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
            Texture2D tex = AssetPreview.GetAssetPreview(go);
            if (tex) GUI.DrawTexture(GUILayoutUtility.GetRect(300, 300, 300, 300), tex, ScaleMode.ScaleToFit);
            EditorGUILayout.EndVertical();
            FixValues(vehicle);
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndScrollView();
        Repaint();
    }

    private void FixValues(Vehicle v)
    {
        var vd = v.GetComponent<VehicleData>();
        if (vd.maxLife < 0) vd.maxLife = 0;
        if (vd.currentLife < 0) vd.currentLife = 0;
        if (vd.currentLife > vd.maxLife) vd.currentLife = vd.maxLife;
        if (v.topSpeed < 0) v.topSpeed = 0;
        if (v.downForce < 0) v.downForce = 0;
        if (v.maxForce < 0) v.maxForce = 0;
        if (v.brakeForce < 0) v.brakeForce = 0;
        if (v.maximumTurn < 0) v.maximumTurn = 0;
        if (v.minimumTurn < 0) v.minimumTurn = 0;
        if (v.nitroPower < 0) v.nitroPower = 0;
    }
}
