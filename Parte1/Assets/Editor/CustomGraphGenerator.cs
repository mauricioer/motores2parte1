﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor(typeof(GraphGenerator))]
public class CustomGraphGenerator : Editor
{
    private GraphGenerator _target;

    private void OnEnable()
    {
        _target = (GraphGenerator)target;
    }

    public override void OnInspectorGUI()
    {
        Show();
        FixValues();
        Repaint();
    }

    private void Show()
    {
        _target.nodePrefab = (Node)EditorGUILayout.ObjectField("Node Prefab", _target.nodePrefab, typeof(Node));
        _target.nodePrefab.nodeRadius = EditorGUILayout.FloatField("Node Radius", _target.nodePrefab.nodeRadius);
        _target.nodePrefab.GetComponent<SphereCollider>().radius = _target.nodePrefab.nodeRadius;
        EditorGUILayout.BeginHorizontal();
        var width = _target.width;
        var heigth = _target.height;
        var distance = _target.distanceBetweenNodes;
        _target.width = EditorGUILayout.IntField("Width", _target.width);
        _target.height = EditorGUILayout.IntField("Height", _target.height);
        EditorGUILayout.EndHorizontal();
        _target.distanceBetweenNodes = EditorGUILayout.FloatField("Distance between nodes", _target.distanceBetweenNodes);
        var vec3 = _target.initialPivot;
        _target.initialPivot = EditorGUILayout.Vector3Field("Initial point", _target.initialPivot);
        if (width != _target.width || heigth != _target.height || _target.initialPivot != vec3 || _target.distanceBetweenNodes != distance)
        {
            if (_target.graph.Count > 0) Calculate();
        }
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate graph"))
        {
            Calculate();
        }
        if (GUILayout.Button("Clear"))
        {
            _target.ClearGraph();
        }
        EditorGUILayout.EndHorizontal();
    }

    private void Calculate()
    {
        var go = new GameObject();
        DestroyImmediate(go);
        _target.canCalculate = true;
    }

    private void FixValues()
    {
        if (_target.nodePrefab.nodeRadius < 0) _target.nodePrefab.nodeRadius = .1f;
        if (_target.width < 0) _target.width = 0;
        if (_target.height < 0) _target.height = 0;
    }
}
